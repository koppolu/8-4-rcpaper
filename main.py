"""
file: main.py

purpose: main.py serves as the base for execution of tests relating Mackey Glass. 
         
         Can disable Hyperparam optimization to unit test the code to ensure it runs correctly. 

#link to methods for finding Execution Time: https://pynative.com/python-get-execution-time-of-program/
"""


import os
import sys
# Get the current working directory
current_directory = os.getcwd()

# Append the current working directory to sys.path
sys.path.append(current_directory)

import CONFIG_TESTS as ct #config_tests

#Importing classes used to test different FRAMEWORKS

if(ct.SYSTEM_FRAMEWORK_CODE == 'saane_RcTorch' or ct.SYSTEM_FRAMEWORK_CODE == 'doubs_RcTorch'):
    from RcTorch_class import RcTorch
    pass

from ReservoirPy_class import ReservoirPy
from pyRCN_class import pyRCN
from Pytorch_ESN_Class import Pytorch

#from ReservoirJulia_Class import ReservoirJulia
if(ct.SYSTEM_FRAMEWORK_CODE == 'rhone_julia' or ct.SYSTEM_FRAMEWORK_CODE == 'ticino_julia'):
    from ReservoirJulia_Class import ReservoirJulia
    pass

# Non framework imports
import matplotlib.pyplot as plt
import csv

#___________________________________________________________________________________________________________

#Function: Run Tests on Framework
def Tests(Framework_RC, Framework_Name, Optimize_Choice):
    from CONFIG_TESTS import RES_SIZE

    Av_Time_List = []
    Total_Time_Error = []

    Av_Timestep_list = []
    Timestep_error = []


    #Loop that collects all data of a Framework
    for res_size in RES_SIZE:
        
        #1. Hyperparameter Optimization is completed during init
        reservoir = Framework_RC(res_size, Framework_Name, Optimize_Choice)
        #NEED to change pyRCN potential leakage values
        reservoir.save_opt_hyperparams()

        #2.Produce Basic Accuracy Data for 100 Node RC
        if (res_size == 100):
            reservoir.Create_Data_Files() 
            reservoir.Basic_Accuracy_Graphic()
            pass



        #3. Data forTimesteps to get to 0.05 nrmse - NO MONTE CARLO YET
        reservoir.Find_Min_Timesteps()

        #4. NRMSE vs Timesteps Graphic
        reservoir.NRMSE_vs_Timesteps_Graphic()

        #5. Total Time Test
        train_time_results = reservoir.Time_Training()

        #5a. storing total training time results
        total_train_time_results = train_time_results[0:3] #extracts everything besides timestep specific data
        rc_total_train_time_file = "Data/Total_Train_Time/" + Framework_Name + '_Total_Train_Time.csv'
        #append Total Time Testresults to file
        with open(rc_total_train_time_file, 'a') as f_object:
            from csv import writer
            writer_object = writer(f_object)
            writer_object.writerow(total_train_time_results)
            f_object.close()
            pass

        Av_Time_List.append(total_train_time_results[1])
        Total_Time_Error.append(total_train_time_results[2])

        #5b. storing timestep length results, saving in Total_Train_time file
        timestep_results = [train_time_results[0]] + train_time_results[3:5]
        rc_timestep_file = "Data/Total_Train_Time/" + Framework_Name + '_timestep.csv'
        #append Total Time Testresults to file
        with open(rc_timestep_file, 'a') as f_object:
            from csv import writer
            writer_object = writer(f_object)
            writer_object.writerow(timestep_results)
            f_object.close()
            pass

        Av_Timestep_list.append(timestep_results[1])
        Timestep_error.append(timestep_results[2])

        pass #End of for loop going over each RC

    #6. PLOTTING total training time and timestep results on separate graphs

    #6a. Plot total time to train and std_error
    plt.clf()
    fig, ax = plt.subplots()

    ax.errorbar(RES_SIZE, Av_Time_List,
                yerr=Total_Time_Error,
                alpha=0.5,
                ecolor='black',
                capsize=10)


    ax.set_xlabel('Reservoir Size')
    ax.set_ylabel('Total Train Time (Sec)')
    ax.set_title(Framework_Name + ' Graph: Total Training Time Length')

    plt.savefig('Data/Total_Train_Time/' + Framework_Name + '_:_total-time_plot.png')
    plt.savefig('Data/Total_Train_Time/' + Framework_Name + '_:_total-time_plot.eps', format='eps')


    #6b. Plot total TIMESTEP LENGTH and std_error
    plt.clf()
    fig, ax = plt.subplots()

    ax.errorbar(RES_SIZE, Av_Timestep_list,
                yerr=Timestep_error,
                alpha=0.5,
                ecolor='black',
                capsize=10)


    ax.set_xlabel('Reservoir Size')
    ax.set_ylabel('Timestep Length (Sec)')
    ax.set_title(Framework_Name + ' Graph: Individual Timestep Length')

    plt.savefig('Data/Total_Train_Time/' + Framework_Name + '_:_timestep_plot.png')
    plt.savefig('Data/Total_Train_Time/' + Framework_Name + '_:_timestep_plot.eps', format='eps')

    pass #end of function
        
#___________________________________________________________________________________________________________
#___________________________________________________________________________________________________________
#Main Point of Execution

Optimize_Choice = input("Do you want to run the w/o Hyperoptimization for Fast testing. If so, press F, else press any other key: ")

if(Optimize_Choice == 'F'): Optimize_Choice = False
else: Optimize_Choice = True

#Framework_dict = {
#  "ReservoirPy": ReservoirPy,
#  "Pytorch": Pytorch,
#  "pyRCN": pyRCN,
#  "RcTorch": RcTorch,
#  "ReservoirJulia": ReservoirJulia
#}


#CALLS: Calling Tests FUNCTION to test each framework

if(ct.SYSTEM_FRAMEWORK_CODE == 'rhine_respy'):
    Tests(ReservoirPy, 'ReservoirPy', Optimize_Choice)
    pass

if(ct.SYSTEM_FRAMEWORK_CODE == 'saane_pyRCN' or ct.SYSTEM_FRAMEWORK_CODE == 'rhone_pyRCN'):
    Tests(pyRCN, 'pyRCN', Optimize_Choice)
    pass

if(ct.SYSTEM_FRAMEWORK_CODE == 'saane_RcTorch' or ct.SYSTEM_FRAMEWORK_CODE == 'doubs_RcTorch'):
    Tests(RcTorch, 'RcTorch', Optimize_Choice)
    pass

if(ct.SYSTEM_FRAMEWORK_CODE == 'rhone_pytorch'):
    Tests(Pytorch, 'Pytorch', Optimize_Choice)
    pass

if(ct.SYSTEM_FRAMEWORK_CODE == 'rhone_julia' or ct.SYSTEM_FRAMEWORK_CODE == 'ticino_julia'):
    Tests(ReservoirJulia, 'ReservoirJulia', Optimize_Choice)
    pass



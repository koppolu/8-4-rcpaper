import docx
import csv
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import linregress

def Add_Framework_to_Graph(framework_file, framework_name, line_color):

    # Initialize empty lists to store your data
    x_data = []
    y_data = []
    error_data = []

    # Read the data from the CSV file
    with open(framework_file, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader)  # Skip the header row if it exists
        for row in csvreader:
            x_data.append(float(row[0]))
            y_data.append(float(row[1]))
            error_data.append(float(row[2]))

    # Convert lists to numpy arrays
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    error_data = np.array(error_data)

    # Create the plot with error bars
    plt.errorbar(x_data, y_data, yerr=error_data, fmt='o', label=framework_name, color = line_color)

    best_equation = Find_Best_Fit(x_data, y_data)
    x_fit_line = np.linspace(min(x_data), max(x_data), 100)
    plt.plot(x_fit_line, best_equation(x_fit_line), label= framework_name + 'Best Fit', color = line_color)
    pass



def Find_Best_Fit(x_data, y_data):

    degree_set = [1, 2, 3, 'exp(x)']

    def exponential_function(x, a, b):
        return a * np.exp(b * x)

    best_equation = 0
    current_equation = 0 
    r_squared_best = 0
    x_fit = x_data

    for degree in degree_set: 

        if degree == 'exp(x)':
            params, covariance = curve_fit(exponential_function, x_data, y_data)
            a, b = params
            current_equation = np.poly1d([a, b], variable='exp(x)')

        else:
            coefficients = np.polyfit(x_data, y_data, degree)
            current_equation = np.poly1d(coefficients)
        
        y_fit = current_equation(x_fit)

        slope, intercept, r_value, p_value, std_err = linregress(y_data, y_fit)
        r_squared_current = r_value ** 2

        if(degree == 1): 
            best_equation = current_equation
            r_squared_best = r_squared_current
            pass

        if(r_squared_current > r_squared_best): 
            r_squared_best = r_squared_current
            best_equation = current_equation
            pass 

        pass

    print(best_equation)
    return best_equation




#________________________________________________________________________________________________________________
#Execution Begins Here

Add_Framework_to_Graph('Pytorch_Total_time.csv', 'pytorch-esn', 'red')
Add_Framework_to_Graph('Respy_Training_Total.csv', 'Reservoirpy', 'blue')
plt.xlabel('Reservoir Size')
plt.ylabel('Total Training Time (seconds)')
plt.title('Comparison of Total Training Time of Multiple Reservoir Computing Frameworks')
plt.legend()
plt.grid(True)

# Show the plot
plt.show()


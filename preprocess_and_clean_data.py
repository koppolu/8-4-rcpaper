### !!! CANNOT import this file anywhere but FRAMEWORK SUPERCLASS, else risk changing the value of GLOBAL VARIABLES: DATE_TIME

import os
import numpy as np
import shutil

from sklearn.model_selection import train_test_split
from pyrcn.datasets import load_digits

#Global Variables
Mackey_Glass_txt_FilePath = "Mackey_Glass.txt"

#___________________________________________________________________________________________________________
#___________________________________________________________________________________________________________
#Creating Mackey Glass Data Set
def Mackey_Glass_Data_Processor():
    #Reading in data as list of strings (each index is 1 line in the file)
    data_file = open(Mackey_Glass_txt_FilePath, "r")
    raw_data_strings = data_file.readlines()
    float_data = []

    for str_number in raw_data_strings:
        float_data.append(float(str_number.split("\n")[0]))

    #turning mackey glass array into numpy array
    mackey_glass_final = np.asarray(float_data).reshape(-1,1) #reservoirpy expects array to be in a specific shape
    #print(mackey_glass_final.shape) #debug

    #Print Data Set as Graph
    
    #plt.figure(figsize=(10, 3))
    #plt.title("Mackey Glass Data")
    #plt.xlabel("First 200 Timesteps (10000 total in dataset)")
    #plt.plot(mackey_glass_final[0:200], label="Mackey_Glass_Data_Set", color="blue")
    #plt.legend()
    #plt.show()

    return mackey_glass_final

#Creating Doublescroll Data Set
def Doublescroll_Processor():
    from reservoirpy.datasets import doublescroll
    import matplotlib.pyplot as plt

    timesteps = 4000
    x0 = [0.37926545, 0.058339, -0.08167691]
    double_scroll_list = doublescroll(timesteps, x0=x0, method="RK23")

    #fig = plt.figure(figsize=(10, 10))
    #ax  = fig.add_subplot(111, projection='3d')
    #ax.set_title("Double scroll attractor (1998)")
    #ax.set_xlabel("x")
    #ax.set_ylabel("y")
    #ax.set_zlabel("z")
    #ax.grid(False)
    #ax.plot(double_scroll_list[0:2000, 0], double_scroll_list[0:2000, 1], double_scroll_list[0:2000, 2], color=plt.cm.cividis(255*10//timesteps), lw=1.0)
    #plt.show()

    return double_scroll_list

#Creating MNIST Data Set
def MNIST_Processor():
    X_digit_coord, Y_actual_value = load_digits(return_X_y=True, as_sequence=True)
    return X_digit_coord, Y_actual_value
    #1437 training samples, 360 test samples
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    pass

#Find date and time
def Date_Time():
    from datetime import datetime
    now = datetime.now()
    return now.strftime("%d_%m_%H_%M")

#___________________________________________________________________________________________________________
#___________________________________________________________________________________________________________
#Pre-processing is complete: There are functions used for cleaning tasks

def delete_rpy_hyperopt_files():
    directory_path = os.getcwd() + "/hyperopt-multiscroll"
    path_to_hyperopt_json = os.getcwd() + "/hyperopt-multiscroll.config.json"

    try:
        shutil.rmtree(directory_path)
        os.remove(path_to_hyperopt_json)
        print("ReservoirPy Hyperopt files deleted")
    except OSError as e:
        print("The ReservoirPy Hyperopt json files are already deleted")
    
    pass

def build_empty_data_files():
     #0. Used to create data files if not created yet
    paths = ["Data", "Data/Hyperparameters", "Data/Basic_Accuracy", "Data/Total_Train_Time", "Data/_00_5_NRMSE", "Data/Timestep_vs_NRMSE"]

    for path in paths:
        # Check whether the specified paths exists or not
        isExist = os.path.exists(path)
        if not isExist:
        # Create a new directory because it does not exist
            os.makedirs(path)
            print(path + " directory created\n")
            pass
        pass


#Clear out old data files: Hyperparameters, Total_Train_Time
def remove_previous_data():

    print("\n\nRemoving previous data collected")

    delete_rpy_hyperopt_files()

    #Used to remove old hyperparameter files
    def remove_files_in_directory(directory_path):
        for filename in os.listdir(directory_path):
            file_path = os.path.join(directory_path, filename)
            try:
                if os.path.isfile(file_path):
                    os.remove(file_path)
                    print(f"Removed: {file_path}")
                elif os.path.isdir(file_path):
                    print(f"Skipping directory: {file_path}")
            except Exception as e:
                print(f"Error deleting {file_path}: {e}")

    directory_to_clear = "Data/Hyperparameters"
    remove_files_in_directory(directory_to_clear)

    directory_to_clear = "Data/Total_Train_Time"
    remove_files_in_directory(directory_to_clear)

    directory_to_clear = "Data/Timestep_vs_NRMSE"
    remove_files_in_directory(directory_to_clear)

    stop_point = input("Finished clearing un-needed Hyperparameter Files, press any key to continue: ")
    pass
#___________________________________________________________________________________________________________


X_coordinate_set = [] #These 2 data sets are only given values if the user selects to run tests with MNIST
Y_actual_digit_set = []

#Create global that Reservoir_Framework can use: set_mackey_glass, dt_string

Chosen_set = input("Choose one the following Data Sets for testing: 'M': Mackey_Glass, 'D': Double_Scroll_Attractor, 'N': MNIST_Number_Set\nChoice: ")
if Chosen_set == 'M': 
    Chosen_set = 'Mackey_Glass'
    set_data_set = Mackey_Glass_Data_Processor()
    pass

elif Chosen_set == 'D': 
    Chosen_set = 'Double_Scroll'
    set_data_set = Doublescroll_Processor()

elif Chosen_set == 'N': 
    Chosen_set = 'MNIST'
    X_coordinate_set, Y_actual_digit_set = MNIST_Processor()

else: 
    Chosen_set == 'No Data Set Chosen'
    raise SystemExit("No Data Set was chosen, please run again")

print("\nYou chose: " + Chosen_set + "\n")
 



dt_string = Date_Time()
build_empty_data_files()
remove_previous_data()


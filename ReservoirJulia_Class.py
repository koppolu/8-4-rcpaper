import pdb
import numpy as np
import matplotlib.pyplot as plt
#from easyesn import PredictionESN
#import timeit #used to time code
import time #used to time code
from reservoirpy.observables import mse, nrmse, rsquare
import statistics
from abc import ABC, abstractmethod
from preprocess_and_clean_data import delete_rpy_hyperopt_files 


import optuna
#from optuna.integration import PyCallPruningCallback
import numpy as np
import julia
from julia import Main
julia.install()
# Install required Julia packages
Main.eval("""
using Pkg
Pkg.add("ReservoirComputing")
Pkg.add("StatsBase")
using ReservoirComputing
using StatsBase
""")




from Framework_Superclass import Reservoir_Framework

class ReservoirJulia(Reservoir_Framework): #superclass containing in brackets

    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize= True):
        
        super().__init__(Set_Num_Nodes, Framework_Name, Optimize) #must call init function of superclass

        self.X_train = self.data_set_final[0:self.training_timesteps].T 
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1].T
        
        self.training_leakage_rate = 0.9

         

        #Optimize set to TRUE when collecting results
        if(self.optimize == True):
            self.ridge = Main.StandardRidge(1e-7)
            self.best_params = self.Optimize_Hyperparam()
            # Initialize Ridge regression model with regularization parameter
            self.reservoir = Main.RandSparseReservoir(self.reservoir_nodes, radius = self.best_params['spectral_radius'], sparsity = 1 - (self.best_params['density']))
            # Create the ESN
            self.esn = Main.ESN(self.X_train,reservoir=self.reservoir, reservoir_driver = Main.RNN(leaky_coefficient=self.best_params['leakage_rate']))
            self.training_leakage_rate = self.best_params['leakage_rate']
            self.opt_hyperparams_list = [self.reservoir_nodes, self.best_params['spectral_radius'], self.best_params['leakage_rate'], self.best_params['density']]
            pass
        
        #The case where optimize = False is only for UNIT TESTS
        else: 

            # Initialize Ridge regression model with regularization parameter
            self.ridge = Main.StandardRidge(1e-7)
            self.reservoir = Main.RandSparseReservoir(self.reservoir_nodes, radius = 0.6, sparsity = 0.5)
            # Create the ESN
            self.esn = Main.ESN(self.X_train,reservoir=self.reservoir, reservoir_driver = Main.RNN(leaky_coefficient=0.9))        


            pass

    def objective1(self, trial):
        # Define the hyperparameters to optimize
        spectral_radius = trial.suggest_uniform("spectral_radius", 0.1, 1.0)
        leakage_rate = trial.suggest_uniform("leakage_rate", 0.1, 1.0)
        density = trial.suggest_uniform("density", 0.1, 0.9)

        # Update the hyperparameters of the reservoir
        self.reservoir = Main.RandSparseReservoir(self.reservoir_nodes, radius=spectral_radius, sparsity=(1-density))

        # Create the ESN with updated hyperparameters
        self.esn = Main.ESN(self.X_train, reservoir=self.reservoir, reservoir_driver=Main.RNN(leaky_coefficient=leakage_rate))

        # Train the ESN
        self.Train()

        # Test the ESN and compute NRMSE
        self.test_results = self.Test()

        return self.test_results['nrmse']


    def Optimize_Hyperparam(self, n_trials=100):
    #override abstract Optimize_Hyperparam

#######

   
        study = optuna.create_study(direction="minimize")
        #callback = PyCallPruningCallback(Main)
        #objective1 more control on all params.
        study.optimize(self.objective1, n_trials=n_trials)#, callbacks=[callback])

        best_trial = study.best_trial

        #print("\n\n\n\nDebug: Best trial:", study.best_trial.params)
        #print("Best NRMSE:", study.best_value)
        
        #input("Debug: Successful Run!!! Hyperparam Optimization Works")

        return study.best_trial.params

    #######
    

    # Override the Train method
    def Train(self):
        self.X_train = self.data_set_final[0:self.training_timesteps].T 
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1].T
        self.esn = Main.ESN(self.X_train,reservoir=self.reservoir, reservoir_driver = Main.RNN(leaky_coefficient=self.training_leakage_rate))
        # Train the ESN
        self.output_layer = Main.train(self.esn, self.Y_train, self.ridge)
        pass

    def compute_nrmse(self, y_true, y_pred):
        mse = np.mean((np.transpose(y_true) - y_pred)**2)
        rmse = np.sqrt(mse)
        nrmse = rmse / (np.max(y_true) - np.min(y_true))
        return nrmse

    # Override the Test method
    def Test(self):
        # Set the actual output data
        Y_actual = self.data_set_final[self.test_set_begin + 1: self.test_set_end + 1]
    
        # Predict the output using the ESN
        prediction_len = self.test_set_end - self.test_set_begin
        prediction = Main.Predictive(self.data_set_final[self.test_set_begin: self.test_set_end].T)

        Y_pred = self.esn(prediction, self.output_layer)
        
        self.calculated_nrmse = self.compute_nrmse(Y_actual, Y_pred)#[:prediction_len,:])
        Y_pred = Y_pred.T
        self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nrmse': self.calculated_nrmse}

        return self.test_results
    
    
    # Override the Reset_Reservoir method
    def Reset_Reservoir(self):

        # Initialize Ridge regression model with regularization parameter
        self.ridge = Main.StandardRidge(1e-7)
        self.reservoir = Main.RandSparseReservoir(self.reservoir_nodes, radius = 0.6, sparsity = 0.5)
        # Create the ESN
        self.esn = Main.ESN(self.X_train,reservoir=self.reservoir, reservoir_driver = Main.RNN(leaky_coefficient=0.9)) 
        pass

"""
import numpy as np
from rctorch import *
import torch
from preprocess_and_clean_data import set_mackey_glass


training_timesteps = 100

X_train = set_mackey_glass[0:training_timesteps] 
X_train = X_train.astype(np.double)
Y_train = set_mackey_glass[1:training_timesteps + 1] 
Y_train = Y_train.astype(np.double)



hps = {'connectivity': 0.4,
           'spectral_radius': 0.5,
           'n_nodes': 202,
           'regularization': 1.69,
           'leaking_rate': 0.6,
           'bias': 0}

my_rc = RcNetwork(**hps, random_state = 210, feedback = True)

#fitting the data:
my_rc.fit(X = X_train, y = Y_train)

#making our prediction
#score, prediction = my_rc.test(X = force_test, y = target_test)
"""



#import packages
#from RcTorch import *
from rctorch import *
import torch
from rctorch.data import final_figure_plot as phase_plot

from matplotlib.pyplot import cm
from matplotlib import cm
import matplotlib.pyplot as plt

import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)
Mackey_Glass_txt_FilePath = "Mackey_Glass.txt"


def Data_Processor():
    #Reading in data as list of strings (each index is 1 line in the file)
    data_file = open(Mackey_Glass_txt_FilePath, "r")
    raw_data_strings = data_file.readlines()
    float_data = []
    for str_number in raw_data_strings:
        float_data.append(float(str_number.split("\n")[0]))
    #turning mackey glass array into numpy array
    mackey_glass_final = np.asarray(float_data).reshape(-1,1) #reservoirpy expects array to be in a specific shape
    return mackey_glass_final

set_mackey_glass = Data_Processor()

training_timesteps = 100

X_train = set_mackey_glass[0:training_timesteps] 
X_train = torch.tensor(X_train, dtype=torch.float32)
X_train = X_train.view(-1)

Y_train = set_mackey_glass[1:training_timesteps + 1] 
Y_train = torch.tensor(Y_train, dtype=torch.float32)
Y_train = Y_train.view(-1)




print(X_train)


#_______Hyperparameter Optimization_________

bounds_dict = {"log_connectivity" : (-2.5, 0), 
               "spectral_radius" : (0, 1.25),
               "n_nodes" : (100,200),
               "log_regularization" : (-5, 1),
               "leaking_rate" : (0, 1),
               "bias": (-1,1)
               }


rc_specs = { "feedback" : True,
             "reservoir_weight_dist" : "uniform",
             "output_activation" : "tanh",
             "random_seed" : 209}

rc_bo = RcBayesOpt( bounds = bounds_dict, 
                    scoring_method = "nmse",
                    n_jobs = 4,
                    cv_samples = 1,
                    initial_samples = 25,
                    **rc_specs
                    )
opt_hps = rc_bo.optimize( n_trust_regions = 4, 
                                  max_evals = 500, 
                                  scoring_method = "nmse",
                                  y = Y_train)

#________End_Hyperopt_______________________

my_rc = RcNetwork(**opt_hps, feedback = True)


my_rc.fit(y = Y_train)


X_test = set_mackey_glass[100:200] 
X_test = torch.tensor(X_test, dtype=torch.float32)
X_test = X_test.view(-1)

Y_test = set_mackey_glass[101:201] 
Y_test_array = Y_test
Y_test = torch.tensor(Y_test, dtype=torch.float32)
Y_test = Y_test.view(-1)

score, prediction = my_rc.test(y  = Y_test, scoring_method='nrmse')

Y_pred_array = prediction.numpy()

# If you want a 1D array, you can reshape it
Y_pred_array = Y_pred_array.reshape(-1)

 #Visualization
plt.clf()
plt.figure(figsize=(10, 3))
plt.title("Accuracy Graphic: Predicted and Actual Mackey_Glass Timeseries.")
plt.xlabel("$t$")
plt.plot(Y_pred_array, label="Predicted ", color="blue")
plt.plot(Y_test_array, label="Real ", color="red")
plt.legend()
plt.show() 


print("score: ", score)
print("prediction: ", prediction)

#print("success")
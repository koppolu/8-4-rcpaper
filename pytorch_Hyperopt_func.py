import numpy as np
import matplotlib.pyplot as plt
#from easyesn import PredictionESN
#import timeit #used to time code
import time #used to time code
from reservoirpy.observables import mse, nrmse, rmse
import statistics
from abc import ABC, abstractmethod
import optuna
from optuna.trial import TrialState


#Global Variables
Mackey_Glass_txt_FilePath = "Mackey_Glass.txt"
#Creating Data Set
def Data_Processor():
    #Reading in data as list of strings (each index is 1 line in the file)
    data_file = open(Mackey_Glass_txt_FilePath, "r")
    raw_data_strings = data_file.readlines()
    float_data = []

    for str_number in raw_data_strings:
        float_data.append(float(str_number.split("\n")[0]))

    #turning mackey glass array into numpy array
    mackey_glass_final = np.asarray(float_data).reshape(-1,1) #reservoirpy expects array to be in a specific shape
    print(mackey_glass_final.shape) #debug

    #Print Data Set as Graph
    
#HC-    plt.figure(figsize=(10, 3))
#HC-    plt.title("Mackey Glass Data")
#HC-    plt.xlabel("First 200 Timesteps (10000 total in dataset)")
#HC-    plt.plot(mackey_glass_final[0:200], label="Mackey_Glass_Data_Set", color="blue")
#HC-    plt.legend()
#HC-    plt.show()

    return mackey_glass_final
#Create global that Reservoir_Framework can use
mackey_glass_final = Data_Processor()

set_no_nodes = 100

def change_no_nodes(value):
    global set_no_nodes 
    set_no_nodes = value
    pass

#Super class for all Reservoir Frameworks
class Reservoir_Framework(ABC): #must pass in ABC parameter to make class abstract

    def __init__(self):

        self.training_timesteps = 300
        self.test_set_begin = 9000
        self.test_set_end = self.test_set_begin + 500
        
        self.X_train = mackey_glass_final[0:self.training_timesteps] 
        self.Y_train = mackey_glass_final[1:self.training_timesteps + 1] 
        
        self.calculated_nrmse = -1
        self.calculated_mse = -1

        self.reservoir_nodes = set_no_nodes
        self.leakage_rate =0.5
        self.spectral_radius =0.9 
        
        self.washout = [0]
        self.input_size = self.output_size = 1
        self.hidden_size = self.reservoir_nodes
        self.loss_fcn = torch.nn.MSELoss()
 
        pass

#abstract methods
    @abstractmethod
    def Train(self):
        pass

    @abstractmethod
    def Test(self):
        pass

    @abstractmethod
    def Reset_Reservoir(self):
        pass

#END of abstract methods
#_________________________________________________________________________________________________________________________________________________

#Non-Abstract methods


    #This function is used to find the minimum number of timesteps it takes to Train to 100% accuracy
    def Find_Min_Timesteps(self):
        
        lowest_possible_timesteps_flag = False #this is set to true when reaching lowest number of timesteps with 0.01 nrmse
        nrmse_0_01_flag = False #used to check nrmse of previous_timestep training was 0.01 when the current nrmse is greater

        previous_timesteps = -1 #prev. number of timesteps used to train previous reservoir

        while lowest_possible_timesteps_flag == False:
            self.Reset_Reservoir()
            self.Train()
            self.calculated_nrmse = self.Test()
            #DEBUG
            print("\nnrmse: ", self.calculated_nrmse)

            #if it hasn't reached required accuracy add more timesteps, OR if previous timesteps had reached required accuracy exit
            if self.calculated_nrmse > 0.01:
                if nrmse_0_01_flag == True:
                    self.training_timesteps = previous_timesteps
                    lowest_possible_timesteps_flag = True
                    pass

                else:    
                    previous_timesteps = self.training_timesteps
                    self.training_timesteps = self.training_timesteps + 50
                    pass

                pass
            #if it has reached required accuracy check to see if less timesteps would still work 
            else:
                nrmse_0_01_flag = True
                previous_timesteps = self.training_timesteps
                self.training_timesteps = self.training_timesteps - 1 
                pass

            #DEBUG
            """
            if self.calculated_nrmse < 0.1:
                lowest_possible_timesteps_flag = True
                print("\nnrmse: " + str(self.calculated_nrmse))
            """


            pass
        #will not be used in paper, but will be used by to determine if same timesteps for all RC 
        print("Personal Metric: The minimum no. of timesteps it takes to reach accuracy of 0.01 nrmse is: " + str(self.training_timesteps))
        return self.training_timesteps



#_________________________________________________________________________________________________________________________________________________
                

    #may implement in base  
    #Only call after completing Find_Min_Timesteps
    def Time_Training(self):
        self.Reset_Reservoir()

        #overall_training_time = timeit.timeit(stmt="self.Train()", globals=globals(), number=1)

        #used to time Training (not sure if I should be using this method)
        #link to methods for finding Execution Time: https://pynative.com/python-get-execution-time-of-program/
        
        
        #--------Monte Carlo Simulation for Calculating overall training time------#
        monte_carlo_sim_size = 10 #set to 1000 when generating data
        overall_training_time_array = [0] * monte_carlo_sim_size
        print(len(overall_training_time_array))

        i = 0
        while i < monte_carlo_sim_size:
            start_time =  time.process_time()
            self.Train()
            end_time =  time.process_time()
            overall_training_time_array[i] = end_time - start_time
            i = i + 1

        print("Training time of all trials before averaging: ",overall_training_time_array) #will comment out when generating data for paper
        print("Standard Deviation all trials: ", statistics.stdev(overall_training_time_array))
        
        train_step_array = np.array(overall_training_time_array) / self.training_timesteps
        av_training_time = sum(overall_training_time_array) / len(overall_training_time_array)
        #--------Monte Carlo Simulation for Calculating overall training time------#



        print("\nRelevent: It takes ", av_training_time, " to train  to 0.01 nrmse (over ", self.training_timesteps, " timesteps.)\n")
        print("Relevent: Time per Train step: ", av_training_time / self.training_timesteps, "\n")
        print("std dev of Time per Trainstep: ", np.std(train_step_array))
        
        return av_training_time




import torch
#torch.set_deterministic(True)
#torch.use_deterministic_algorithms(True)
#torch.manual_seed(11663260459414417523)
print("Using PyTorch deterministic mode with seed: ", torch.initial_seed())
import torch.nn as nn
from sklearn.linear_model import Ridge
from torchesn.nn import ESN as ESN
import numpy as np
import torch.nn
from torchesn import utils
device = torch.device('cpu')
dtype = torch.double
torch.set_default_dtype(dtype)





class ReservoirPytorch(Reservoir_Framework): #superclass containing in brackets

    def __init__(self):
        super().__init__() #must call init function of superclass
        #data
        print(self.X_train.shape)
        print(self.Y_train.shape)
        self.trX = torch.from_numpy(self.X_train).unsqueeze(1).to(device)
        self.trY = torch.from_numpy(self.Y_train).unsqueeze(1).to(device)
        self.tsX = torch.from_numpy(mackey_glass_final[self.test_set_begin : self.test_set_end]).unsqueeze(1).to(device)
        self.tsY = torch.from_numpy(mackey_glass_final[self.test_set_begin + 1 : self.test_set_end + 1]).unsqueeze(1).to(device)
        print(self.trX.shape)
        print(self.trY.shape)        
        pass

    #override abstract Train method
    def Train(self):
    # Training
        trY_flat = utils.prepare_target(self.trY.clone(), [self.trX.size(0)], self.washout)
    
        self.model.to(device)
    
        self.model(self.trX, self.washout, None, trY_flat)
        self.model.fit()
        self.output, self.hidden = self.model(self.trX, self.washout)
        pass
    def Test(self):    
         # Test
         self.output, self.hidden = self.model(self.tsX, [0], self.hidden)
        #NRMSE
         test_loss = torch.sqrt(self.loss_fcn(self.output, self.tsY)) / (torch.max(self.output) - torch.min(self.output))
         print("Testing NRMSE:", test_loss.item())
         self.calculated_nrmse = test_loss.item()
         return self.calculated_nrmse

   


    #override abstract Reset method
    def Reset_Reservoir(self,trial):
        self.model = ESN(
            self.input_size,
            self.hidden_size,
            self.output_size,
            spectral_radius=trial.suggest_uniform("spectral_radius", 0.0, 1.0),
            leaking_rate=trial.suggest_uniform("leaking_rate", 0.0, 1.0),
            density=trial.suggest_uniform("density", 0.0, 1.0),
        )    
        pass
print("\n\nStart\n\n")
#HCdef objective(trial: optuna.Trial):
#HC        model = ReservoirPytorch()
#HC        model.Reset_Reservoir(trial)
#HC        model.Train()
#HC        nrmse = model.Test()
#HC        return nrmse

def objective(trial):
    model = ReservoirPytorch()
    model.Reset_Reservoir(trial)
    print("Reservoir Nodes",model.reservoir_nodes)
    # Check for NaN or infinite values in the input data
    if torch.isnan(model.trX).any() or torch.isinf(model.trX).any():
        return float('inf')
    
    model.Train()
    nrmse = model.Test()
    return nrmse


def COMPLETE_OPT():
    is_opt_done = False

    while is_opt_done == False:
        try:
            study = optuna.create_study(direction="minimize")
            study.optimize(objective, n_trials=200)
        except:
            is_opt_done = False
        else:
            is_opt_done = True
        pass

    best_params = study.best_params
    print("\n\n\n\n\n\n\nBest Hyperparameters:", best_params)
    print("\n\n\n\n\n\n")
    return best_params
    
def Hyperopt_PyTorch_ESN(val):
    change_no_nodes(val)
    best_params = COMPLETE_OPT()
    return best_params
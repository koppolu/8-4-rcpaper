from ReservoirPy_class import ReservoirPy
from pyRCN_class import pyRCN
import matplotlib.pyplot as plt
import csv

#reservoir = ReservoirPy(100, 'ReservoirPy', False)

#reservoir.Basic_Accuracy_Graphic()


reservoir = ReservoirPy(100, 'ReservoirPy', False)

reservoir.Train()
test_results = reservoir.Test()
print("error_rate: ", test_results['error_rate'])

#reservoir.Basic_Accuracy_Graphic()
#Import ABC to allow for abstract classes, time for calculating training time

from abc import ABC, abstractmethod
from re import I
import time
from unittest import result #used to time code
import numpy as np
import statistics
import matplotlib.pyplot as plt

import os
import csv
import preprocess_and_clean_data as pc

import math



MIN_NRMSE = 0.05
MONTE_CARLO_SIZE_TOTAL_TRAIN_TIME = 10 #set to 100 when generating data
DATE_TIME = pc.dt_string

MONTE_CARLO_MIN_TIMESTEPS = 3 #May not want to set any higher than 10

#___________________________________________________________________________________________________________
#Super class for all Reservoir Frameworks
class Reservoir_Framework(ABC): #must pass in ABC parameter to make class abstract

#IN CHILD classes Hyperparameter Optimization always occurs in the init
    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize = True):

        self.MAX_TIMESTEPS = 0 #is ALTERED DEPENDING ON THE DATASET WITHIN FRAMEWORK SUPERCLASS INIT

        self.date_time = DATE_TIME
        self.framework_name = Framework_Name
        self.Create_Data_Files()

        self.Data_Set_Name = pc.Chosen_set


        if(self.Data_Set_Name == 'Mackey_Glass'): self.Set_Mackey()
        elif(self.Data_Set_Name == 'Double_Scroll'): self.Set_Double_Scroll()
        elif(self.Data_Set_Name == 'MNIST'): self.Set_MNIST()
        

        self.optimize = Optimize

        self.calculated_nrmse = -1
        self.calculated_mse = -1
        self.test_results = {} #Used to results of Test function

        self.reservoir_nodes = Set_Num_Nodes
        self.leakage_rate = 0.5
        self.spectral_radius = 0.9

        #used for data collection
        self.minimum_timesteps = -1

        self.av_timestep = -1
        self.stand_dev_timestep = -1

        self.av_training_time = -1
        self.stand_dev_train_time = -1
        
        self.opt_hyperparams_list = ["You forgot to create the hyperparam list"]

        pass

    
    #These functions are called by init to set the Data_Set____________________________________________________
    def Set_Mackey(self):
        self.MAX_TIMESTEPS = 2000

        self.training_timesteps = 100
        self.test_set_begin = 9000
        self.test_set_end = self.test_set_begin + 500
        self.data_set_final = pc.set_data_set
        pass

    def Set_Double_Scroll(self):
        self.MAX_TIMESTEPS = 2000

        self.training_timesteps = 1000
        self.test_set_begin = 2000
        self.test_set_end = self.test_set_begin + 1000
        self.data_set_final = pc.set_data_set
        pass

    def Set_MNIST(self):
        self.MAX_TIMESTEPS = 1200

        self.training_timesteps = 200
        self.test_set_begin = 1200
        self.test_set_end = self.test_set_begin + 400
        #3. MNIST
        self.X_coord_digit_set = pc.X_coordinate_set
        self.Y_actual_digit_set = pc.Y_actual_digit_set
        pass
#___________________________________________________________________________________________________________
    #0. Used to create data files if not created yet
    def Create_Data_Files(self):
        paths = ["Data", "Data/Hyperparameters", "Data/Basic_Accuracy", "Data/Total_Train_Time", "Data/_00_5_NRMSE", "Data/Timestep_vs_NRMSE"]

        for path in paths:
            # Check whether the specified paths exists or not
            isExist = os.path.exists(path)
            if not isExist:
            # Create a new directory because it does not exist
                os.makedirs(path)
                print(path + " directory created\n")
                pass
            pass

        #Hyperparameter file creation


        filename = 'Data/Hyperparameters/' + self.framework_name + '_hyperparam' + '.csv' #may add date and time

        isExist = os.path.exists(filename)

        if not isExist:
            fields = ['interal nodes', 'spectral radius', 'leakage rate', 'input scaling'] 
            with open(filename, 'w+') as csvfile: 
                csvwriter = csv.writer(csvfile) 
                csvwriter.writerow(fields) 
                pass

            csvfile.close()
            pass
            
        #Only used to store data for Test 5. Total Time Test
        rc_total_train_time_file = "Data/Total_Train_Time/" + self.framework_name + '_Total_Train_Time.csv'
        isExist = os.path.exists(rc_total_train_time_file) 

        if not isExist:
            fields = ['reservoir size', 'time training', 'std dev']
            with open(rc_total_train_time_file, 'w+') as csvfile: 
                csvwriter = csv.writer(csvfile) 
                csvwriter.writerow(fields) 
                pass

                csvfile.close()
            pass


        pass
#___________________________________________________________________________________________________________

#abstract methods

    #This function is ONLY meant to return the values of the Optimized hyperparams
    #It DOES NOT create a Reservoir Computer

    @abstractmethod
    def Optimize_Hyperparam(self):
        pass

    #Must always be called by instance of Childclass after Optimize_Hyperparam
    def save_opt_hyperparams(self):
        filename = 'Data/Hyperparameters/' + self.framework_name + '_hyperparam' + '.csv'

        
        with open(filename, 'a') as f_object:
            from csv import writer
            writer_object = writer(f_object)
            writer_object.writerow(self.opt_hyperparams_list)
            f_object.close()
            pass

        pass

    @abstractmethod
    def Train(self):
        pass

    @abstractmethod
    def Test(self):
        pass

    @abstractmethod
    def Reset_Reservoir(self):
        pass

#END of abstract methods

#_________________________________________________________________________________________________________________________________________________

#Non-Abstract methods: USED FOR TESTING

    def Mackey_Accuracy_Graphic(self):
        
        #Storing Data
        self.Y_pred_store = np.asarray(self.test_results['Y_pred'])
        self.Y_pred_store.tofile('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Predicted_values.csv', sep = ',') 
        self.Y_actual_store = np.asarray(self.test_results['Y_actual'])
        self.Y_actual_store.tofile('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Actual_values.csv', sep = ',') 
        
        #retreiving data
        self.Y_pred_store = np.loadtxt('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Predicted_values.csv', delimiter=",")
        self.Y_actual_store = np.loadtxt('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Actual_values.csv', delimiter=",")

        #Visualization
        plt.clf()
        plt.figure(figsize=(10, 3))
        plt.title(self.framework_name + " Accuracy Graphic: Predicted and Actual Mackey_Glass Timeseries.")
        plt.xlabel("$t$")
        plt.plot(self.Y_pred_store, label="Predicted ", color="blue")
        plt.plot(self.Y_actual_store, label="Real ", color="red")
        #plt.plot(self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1], label="Real ", color="red")
        plt.legend()
        plt.ylim(-0.5, 0.5)


        #Graph isn't displayed; it is immediately saved
        plt.savefig('Data/Basic_Accuracy/' + self.Data_Set_Name + '_' + self.framework_name + '_' + str(self.reservoir_nodes) + '_basic_accuracy.eps', format='eps')
        plt.savefig('Data/Basic_Accuracy/' + self.Data_Set_Name + '_' + self.framework_name + '_' + str(self.reservoir_nodes) + '_basic_accuracy.png')
        pass

    def Double_Scroll_Accuracy_Graphic(self):
        
        #Storing Data
        self.Y_pred_store = np.array(self.test_results['Y_pred'])
        np.save('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Predicted_values.npy', self.Y_pred_store)
        self.Y_actual_store = np.array(self.test_results['Y_actual'])
        np.save('Data/Basic_Accuracy/Pred_vs_Actual_raw_data/' + self.Data_Set_Name + '_' + self.framework_name + '_Actual_values.npy', self.Y_actual_store)
        
        #retreiving data
        #Actually No Need to Retreive Data

        print("self.Y_pred_store.shape:", self.Y_pred_store.shape)
        print("self.Y_actual_store.shape:", self.Y_actual_store.shape)

        #Actual vs Predicted Double Scroll Attractor
        plt.clf()
        fig = plt.figure(figsize=(10, 10))
        ax  = fig.add_subplot(111, projection='3d')
        ax.set_title("Predicted Double scroll attractor")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        ax.grid(False)
        ax.plot(self.Y_actual_store[0:-1, 0], self.Y_actual_store[0:-1, 1], self.Y_actual_store[0:-1, 2],label='Actual', color='red', lw=1.0)
        ax.plot(self.Y_pred_store[0:-1, 0], self.Y_pred_store[0:-1, 1], self.Y_pred_store[0:-1, 2],label='Predicted', color='blue', lw=1.0)
        plt.legend()
        #plt.show()
        
        plt.savefig('Data/Basic_Accuracy/' + self.Data_Set_Name + '_' + self.framework_name + '_' + str(self.reservoir_nodes) + '_basic_accuracy.eps', format='eps')
        plt.savefig('Data/Basic_Accuracy/' + self.Data_Set_Name + '_' + self.framework_name + '_' + str(self.reservoir_nodes) + '_basic_accuracy.png')

        print("Double Scroll Basic Accuracy Data\n_____________________________________\n")
        print("Y_pred size: ", self.Y_pred_store.size)
        print("Y_actual size: ", self.Y_actual_store.size)
        print("NRMSE: ", self.test_results['nrmse'])
        print('_________________________________')


        pass

    #1. Produces simple accuracy display of previously Hyperopt 100 RC
    def Basic_Accuracy_Graphic(self):

        #100 training timesteps (if this func is called first)
        self.Train()
        self.test_results = self.Test()        

        #Creating file to store raw data
        path = 'Data/Basic_Accuracy/Pred_vs_Actual_raw_data'
        isExist = os.path.exists(path)
        if not isExist:
            # Create a new directory because it does not exist
            os.makedirs(path)
            print(path + " directory created\n")

        #MAY WANT TO RET TO MACKEY_GLASS GRAPHIC_____________________________________________
        #____________________________________________________________________________________

        if self.Data_Set_Name == 'Mackey_Glass': self.Mackey_Accuracy_Graphic()
        elif self.Data_Set_Name == 'Double_Scroll': self.Double_Scroll_Accuracy_Graphic()

        #Saving each nrmse value in separate file
        import csv
        filename = 'Data/Basic_Accuracy/' + self.framework_name + '_' + str(self.reservoir_nodes) + '_basic_accuracy_NRMSE.csv'

        with open(filename, 'w+') as csvfile: 
            csvwriter = csv.writer(csvfile) 
            csvwriter.writerow([self.test_results['nrmse']]) 
            pass

        csvfile.close()       

        #print('\n\n' + self.framework_name + ' Basic Display nmrse: ', self.test_results['nrmse'])

        pass


#_________________________________________________________________________________________________________________________________________________
    #2. Find the minimum number of timesteps it takes to Train to 0.05 nrmse
    def Find_Min_Timesteps(self):

        monte_carlo_min_timestep_array = []
        self.training_timesteps = 30 #Set to low value from 100 to decrease time of this test
        #Do NOT need to reset training_timesteps with each monte carlo run, can start from the previous ideal timestep value
        i = 0
        while i < MONTE_CARLO_MIN_TIMESTEPS:

             

            lowest_possible_timesteps_flag = False #this is set to true when reaching lowest number of timesteps with 0.1 nrmse
            nrmse_00_5_flag = False #used to check nrmse of previous_timestep training was 0.05 when the current nrmse is greater

            previous_timesteps = -1 #prev. number of timesteps used to train previous reservoir

            while lowest_possible_timesteps_flag == False:
                self.Reset_Reservoir()
                self.Train()
                prev_nrmse = self.calculated_nrmse
                self.test_results = self.Test()
                self.calculated_nrmse = self.test_results['nrmse']

                #if it hasn't reached required accuracy add more timesteps, OR if previous timesteps had reached required accuracy exit
                if self.calculated_nrmse > MIN_NRMSE and self.training_timesteps < self.MAX_TIMESTEPS:

                    #if going back to find from lower NRMSE values to get 0.05 NRMSE
                    if nrmse_00_5_flag == True:
                        print("\n\nTest success!\n\n")
                        self.training_timesteps = previous_timesteps
                        self.calculated_nrmse = prev_nrmse
                        lowest_possible_timesteps_flag = True
                        pass

                    else:    
                        previous_timesteps = self.training_timesteps
                        self.training_timesteps = self.training_timesteps + 50
                        pass

                    pass

                #if it has reached required accuracy check to see if less timesteps would still work 
                elif(self.calculated_nrmse <= MIN_NRMSE and self.training_timesteps < self.MAX_TIMESTEPS):
                    nrmse_00_5_flag = True
                    previous_timesteps = self.training_timesteps
                    self.training_timesteps = self.training_timesteps - 1
                    pass

                else:
                    print("FAILED: Did Not reach 0.05 NRMSE")

                
                if(self.training_timesteps < 0): 
                    print("FAILURE: training timesteps reached a value below 0. This is not possible.")
                    raise SystemExit("FAILURE: training timesteps reached a value below 0. This is not possible.")

            self.minimum_timesteps = self.training_timesteps
            monte_carlo_min_timestep_array.append(self.minimum_timesteps) 
            i = i + 1
            pass

        self.stand_dev_min_timesteps = np.std(monte_carlo_min_timestep_array)
        self.av_min_timestep = math.ceil(np.average(monte_carlo_min_timestep_array)) #calculating the ceiling of the minimum timestep
        self.minimum_timesteps = self.av_min_timestep

        import csv
        filename = 'Data/_00_5_NRMSE/' + self.framework_name + '_' + str(self.reservoir_nodes) + '_nodes_00_5_NRMSE_Min_Timestep.csv'

        fields = ['reservoir_size', 'nrmse', 'Timesteps', 'Standard Deviation']
        input_data = [self.reservoir_nodes, self.calculated_nrmse, self.minimum_timesteps, self.stand_dev_min_timesteps] 
        with open(filename, 'w+') as csvfile: 
            csvwriter = csv.writer(csvfile) 
            csvwriter.writerow(fields) 
            csvwriter.writerow(input_data) 
            pass

            csvfile.close()
        print("\n\n\n____________________________________________________\nCompleted Determination of Minimum Timesteps\n\n")
        print("nrmse: ", self.calculated_nrmse)
        
        if(self.minimum_timesteps < self.MAX_TIMESTEPS): print("Personal Metric: The minimum no. of timesteps it takes to reach accuracy of 0.05 nrmse is: " + str(self.training_timesteps))
        else: print("\n\n_________________________\nTimestep measurement is WRONG. Again this measurement is WRONG!\n_________________________\n\n\n")
        return self.minimum_timesteps


#_________________________________________________________________________________________________________________________________________________
    #3. NRMSE vs No. of Timesteps graph. 
    def NRMSE_vs_Timesteps_Graphic(self):

        Timestep_Set = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 1000]
        Timestep_Set = np.asarray(Timestep_Set)
        nrmse_array = np.empty(Timestep_Set.size)

        iter = 0
        for no_timesteps in Timestep_Set:
            self.Reset_Reservoir()
            self.training_timesteps = no_timesteps
            self.Train()
            self.test_results = self.Test()

            #saving data in array
            self.calculated_nrmse = self.test_results["nrmse"]
            nrmse_array[iter] = self.calculated_nrmse
            iter = iter + 1
            pass

        #storing data in file
        nrmse_timestep_file = "Data/Timestep_vs_NRMSE/" + self.framework_name + '_' + str(self.reservoir_nodes) + '_time_vs_nrmse.csv'
        isExist = os.path.exists(nrmse_timestep_file) 

        if not isExist:
            with open(nrmse_timestep_file, 'w+') as csvfile: 
                csvwriter = csv.writer(csvfile) 
                csvwriter.writerow(Timestep_Set)
                csvwriter.writerow(nrmse_array) 
                pass

                csvfile.close()
            pass

        #nrmse_array.tofile('Data/Timestep_vs_NRMSE/' + self.framework_name + '_' + str(self.reservoir_nodes) + ':_Timesteps_vs_NRMSE.csv', sep = ',')
        

        #retreiving data

        with open(nrmse_timestep_file, 'r') as file:
            reader = csv.reader(file)
            rows = [row for row in reader]

        try:
            Timestep_Set = np.array(rows[0], dtype=np.float32)  # Assuming the first row contains numerical data
            nrmse_array = np.array(rows[1], dtype=np.float32)
        except:
            raise Exception("File was not created properly")
        

        #nrmse_array = np.loadtxt('Data/Timestep_vs_NRMSE/' + self.framework_name + '_' + str(self.reservoir_nodes) + ':_Timesteps_vs_NRMSE.csv', delimiter=",")

        #clearing previous figure
        plt.clf()

        #THE GRAPHS CREATED IN THIS FUNCTION ARE NOT USED BY THE REPORT
        fig, ax = plt.subplots()
        ax.set_xscale('log')
        ax.plot(Timestep_Set, nrmse_array)
        ax.set_xlabel('X-axis (log scale)')
        ax.set_ylabel('Y-axis')


        #plt.plot(Timestep_Set, nrmse_array)
        #plt.xlabel("Timesteps")
        #plt.ylabel("nrmse of " + str(self.reservoir_nodes) + " node reservoir")
        plt.savefig('Data/Timestep_vs_NRMSE/' + self.framework_name + '_' + str(self.reservoir_nodes) + '_NRMSE_vs_Timesteps_Graph.eps', format='eps' )
        plt.savefig('Data/Timestep_vs_NRMSE/' + self.framework_name + '_' + str(self.reservoir_nodes) + '_NRMSE_vs_Timesteps_Graph.png')
        pass


#_________________________________________________________________________________________________________________________________________________
    #4. Find the total training time, only call after completing Find_Min_Timesteps
    #THIS is the ONLY test function that does NOT save the data automatically. Data is collected and saved in main.py TESTS function
    #May also want to do Time per Timestep calculation  
    def Time_Training(self):

        #link to methods for finding execution time: https://pynative.com/python-get-execution-time-of-program/
        
        
        #--------monte carlo simulation for calculating overall training time------#
        self.Reset_Reservoir()
        self.training_timesteps = self.minimum_timesteps

        overall_training_time_array = [0] * MONTE_CARLO_SIZE_TOTAL_TRAIN_TIME

        i = 0
        while i < MONTE_CARLO_SIZE_TOTAL_TRAIN_TIME:
            #NEW ADDITION
            self.Reset_Reservoir()
            #may need to add reset call here
            start_time = time.process_time()
            self.Train()
            end_time = time.process_time()
            overall_training_time_array[i] = end_time - start_time
            i = i + 1
            pass
        #--------end of monte carlo sim --------------------------------------------#

        #print("training time of all trials before averaging: ",overall_training_time_array) #will comment out when generating data for paper

        print("\n\n_________________________________________Total Time Training\n____________________________________________\n\n")
        print("standard deviation all trials: ", statistics.stdev(overall_training_time_array))
        
        train_step_array = np.array(overall_training_time_array) / self.training_timesteps
        self.av_training_time = sum(overall_training_time_array) / len(overall_training_time_array)


        print("\nrelevent: it takes ", self.av_training_time, " to train  to 0.05 nrmse, going through", self.training_timesteps, " timesteps.\n")
        self.stand_dev_train_time = np.std(overall_training_time_array)
        print("std dev of total training time: ", self.stand_dev_train_time)

        self.av_timestep = self.av_training_time / self.training_timesteps
        print("\n\nrelevent: time per train step: ", self.av_timestep, "\n")
        
        self.stand_dev_timestep = np.std(train_step_array)
        print("std dev of time per trainstep: ", self.stand_dev_timestep)

        results = self.Test()
        print('Nrmse of last training trial:', results['nrmse'])
        
        #returning total training time and time per timestep 
        return [self.reservoir_nodes, self.av_training_time, self.stand_dev_train_time, self.av_timestep, self.stand_dev_timestep]


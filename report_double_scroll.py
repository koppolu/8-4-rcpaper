import docx
import csv
import matplotlib.pyplot as plt
import numpy as np

from config_tests import RES_SIZE, LIST_OF_FRAMEWORKS_TESTED





#______Used to Create Hyperparameter Table__________________________________________________________________________________________
def read_csv(file_path):
    data = []
    with open(file_path, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            data.append(row)
            pass
    return data

#______Used for both Hyperparam and 0.05 NRMSE table_________________________________________________________________________________
def create_word_table(document, data):
    # Assuming data is a list of lists, where each list represents a row in the table.
    table = document.add_table(rows=len(data), cols=len(data[0]))
    
    for i, row in enumerate(data):
        for j, cell_data in enumerate(row):
            table.cell(i, j).text = cell_data
            pass
        pass

#______Used to Create NRMSE_vs_Timesteps Graphic______________________________________________________________________________________
def create_nrmse_vs_timestep_all_RC_graphic(Framework_Name):
    index = 0
    All_RC_nrmse_array = []
    #Save data into numpy arrays 
    for res_size in RES_SIZE:
        nrmse_timestep_file = "Data/Timestep_vs_NRMSE/" + Framework_Name + '_' + str(res_size) + '_time_vs_nrmse.csv'
        with open(nrmse_timestep_file, 'r') as file: 
            reader = csv.reader(file)
            rows = [row for row in reader]

            try:
                Timestep_Set_X = np.array(rows[0], dtype=np.float32)  
                All_RC_nrmse_array.append(np.array(rows[1], dtype=np.float32))
            except:
                raise Exception("File was not created properly")

        index = index + 1
        pass


    # Create the plot
    plt.clf
    plt.figure(figsize=(8, 6))
    index = 0
    for res_size in RES_SIZE:
        plt.plot(Timestep_Set_X, All_RC_nrmse_array[index], label= str(res_size) + ' nodes')
        index = index + 1
        pass

    # Add labels and a legend
    plt.xlabel('Timesteps')
    plt.ylabel('nrmse')
    plt.title('NRMSE vs Timesteps:' + Framework_Name)
    plt.legend()

    # Save plot
    plt.savefig('Data/Timestep_vs_NRMSE/' + Framework_Name + '_All_RC' + '_NRMSE_vs_Timesteps_Graph.eps', format='eps' )
    plt.savefig('Data/Timestep_vs_NRMSE/' + Framework_Name + '_All_RC' + '_NRMSE_vs_Timesteps_Graph.png')

    pass

#_____________________________________________________________________________________________________________________________________
def create_0_05_nrmse_table(document, Framework_Name):
    
    #Read in data from the different .csv files
    data_titles = np.array(['reservoir_size','nrmse', 'timesteps'])
    data = []
    data.append(data_titles)
    for res_size in RES_SIZE:
        filename = 'Data/_00_5_NRMSE/' + Framework_Name + '_' + str(res_size) + '_nodes_00_5_NRMSE_Min_Timestep.csv'
        with open(filename, 'r') as file:
            reader = csv.reader(file)
            rows = [row for row in reader]

        try:
            data.append(rows[1])
        except:
            raise Exception("File was not created properly")

    #Creates the table
    create_word_table(document, data)
    pass

#_____________________________________________________________________________________________________________________________________
def Add_image_to_rightmost_side(doc, img_path):
    from docx.shared import Inches
    p = doc.add_paragraph()

    p.paragraph_format.left_indent = Inches(-1.0)

    run = p.add_run()
    run.add_picture(img_path, width=Inches(6.81)) 

    pass

    
#___Main Point of Execution___________________________________________________________________________________________________________

# Create an instance of a word document
doc = docx.Document()

doc.add_paragraph('This document was generated using the python-docx module.')
 
#Display Hyperparameter of different RC of different frameworks
doc.add_heading('Hyperparameters', 0)

for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    csv_file_path =  'Data/Hyperparameters/' + FRAMEWORK_NAME + '_hyperparam' + '.csv'
    doc.add_heading(FRAMEWORK_NAME, 2)
    csv_data = read_csv(csv_file_path)
    create_word_table(doc, csv_data)

#Display Basic accuracy Graphic
doc.add_heading('Demonstration of Basic Functionality', 0)
#WILL ADD
for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    doc.add_heading(FRAMEWORK_NAME, 2)
    Add_image_to_rightmost_side(doc, 'Data/Basic_Accuracy/Double_Scroll_' + FRAMEWORK_NAME +'_100_basic_accuracy.png') 


doc.add_heading('Total Training Time', 0)
for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    doc.add_heading(FRAMEWORK_NAME, 2)
    doc.add_picture('Data/Total_Train_Time/' + FRAMEWORK_NAME +'_:_total-time_plot.png')


doc.add_heading('Individual Timestep Length', 0)
for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    doc.add_heading(FRAMEWORK_NAME, 2)
    doc.add_picture('Data/Total_Train_Time/' + FRAMEWORK_NAME +'_:_timestep_plot.png')



doc.add_heading('Timesteps vs NRMSE', 0)
for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    doc.add_heading(FRAMEWORK_NAME, 2)
    create_nrmse_vs_timestep_all_RC_graphic(FRAMEWORK_NAME)
    doc.add_picture('Data/Timestep_vs_NRMSE/' + FRAMEWORK_NAME + '_All_RC' + '_NRMSE_vs_Timesteps_Graph.png')



doc.add_heading('Timesteps to get to 0.05 NRMSE', 0)
for FRAMEWORK_NAME in LIST_OF_FRAMEWORKS_TESTED:
    doc.add_heading(FRAMEWORK_NAME, 2)
    create_0_05_nrmse_table(doc, FRAMEWORK_NAME)

# Now save the document to a location
doc.save('Data/Mackey_Framework_Report.docx')




## THIS opens the report document automatically 
#Open docx file
import subprocess

# Replace 'your_file.docx' with the actual file path
docx_file = 'Data/Mackey_Framework_Report.docx'

try:
    # Open the DOCX file using the default associated application
    subprocess.run(['open', docx_file])  # For macOS
    # If you're using a Windows system, use the following line instead:
    # subprocess.run(['start', '""', docx_file], shell=True)
except FileNotFoundError:
    print(f"Error: '{docx_file}' not found.")
except Exception as e:
    print(f"An error occurred: {e}")
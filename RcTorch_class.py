from Framework_Superclass import Reservoir_Framework
from reservoirpy.observables import nrmse
import matplotlib.pyplot as plt
import numpy as np

from rctorch import *
import torch
from rctorch.data import final_figure_plot as phase_plot

class RcTorch(Reservoir_Framework): #superclass containing in brackets

    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize= True):
        super().__init__(Set_Num_Nodes, Framework_Name, Optimize) #must call init function of superclass

        if(Optimize == False):
            self.temp_hps = {'connectivity': 0.4,
                    'spectral_radius': 0.5,
                    'n_nodes': self.reservoir_nodes,
                    'regularization': 1.69,
                    'leaking_rate': 0.6,
                    'bias': 0}
            
            self.reservoir = RcNetwork(**self.temp_hps, feedback = True)
            self.opt_hyperparams_list = [self.reservoir_nodes, self.temp_hps['spectral_radius'], self.temp_hps['leaking_rate'], self.temp_hps['connectivity'], self.temp_hps['regularization'], self.temp_hps['bias']]
        
        else:
            self.best_params = self.Optimize_Hyperparam()
            self.reservoir = RcNetwork(**self.best_params, feedback = True)
            self.opt_hyperparams_list = [self.reservoir_nodes, self.best_params['spectral_radius'], self.best_params['leaking_rate'], self.best_params['connectivity'], self.best_params['regularization'], self.best_params['bias']]
            pass
        #input("\n\nMust fix train and test and must also add way to save best_params, must see how hyperparam saved for each framework\n\n")


    def Optimize_Hyperparam(self):
        bounds_dict = {"log_connectivity" : (-2.5, 0), 
            "spectral_radius" : (0, 1.25),
            "n_nodes" : self.reservoir_nodes,
            "log_regularization" : (-5, 1),
            "leaking_rate" : (0, 1),
            "bias": (-1,1)
            }


        rc_specs = { "feedback" : True,
                    "reservoir_weight_dist" : "uniform",
                    "output_activation" : "tanh",
                    "random_seed" : 209}

        rc_bo = RcBayesOpt( bounds = bounds_dict, 
                            scoring_method = "nmse",
                            n_jobs = 4,
                            cv_samples = 1,
                            initial_samples = 25,
                            **rc_specs
                            )
        Y_train = self.data_set_final[1:self.training_timesteps + 1] 
        Y_train_tensor = torch.tensor(Y_train, dtype=torch.float32)
        if(self.Data_Set_Name == 'Mackey_Glass'):
            Y_train_tensor = Y_train_tensor.view(-1)
            pass

        self.best_params = rc_bo.optimize( n_trust_regions = 4, 
                                        max_evals = 500, 
                                        scoring_method = "nmse",
                                        y = Y_train_tensor)

        return self.best_params
        

    ####WILL NEED TO FIX TRAIN AND TEST ###
    def Train(self):
        X_train = self.data_set_final[0:self.training_timesteps] 
        X_train_tensor = torch.tensor(X_train, dtype=torch.float32)
        

        Y_train = self.data_set_final[1:self.training_timesteps + 1] 
        Y_train_tensor = torch.tensor(Y_train, dtype=torch.float32)

        if(self.Data_Set_Name == 'Mackey_Glass'):
            X_train_tensor = X_train_tensor.view(-1)
            Y_train_tensor = Y_train_tensor.view(-1)
            pass

        self.reservoir.fit(y = Y_train_tensor)
        pass

    def Test(self):
        X_test = self.data_set_final[self.test_set_begin : self.test_set_end] 
        X_test_tensor = torch.tensor(X_test, dtype=torch.float32)

        Y_actual = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1] 
        Y_test_tensor = torch.tensor(Y_actual, dtype=torch.float32)
        
        if(self.Data_Set_Name == 'Mackey_Glass'):
            X_test = X_test_tensor.view(-1)
            Y_test_tensor = Y_test_tensor.view(-1)
            pass

        score, prediction = self.reservoir.test(y  = Y_test_tensor, scoring_method='nrmse')

        Y_pred = prediction.numpy()

        if(self.Data_Set_Name == 'Mackey_Glass'):
            Y_pred = Y_pred.reshape(-1)
            Y_actual = Y_actual.reshape(-1)
            pass

        #this is used to calculate nrmse and mse
        self.calculated_nrmse = nrmse(Y_pred, Y_actual)
        self.calculated_mse = (Y_pred, Y_actual)

        self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nrmse': self.calculated_nrmse}

        return self.test_results
        pass

    def Reset_Reservoir(self):
        self.reservoir = RcNetwork(**self.best_params, feedback = True)
        pass
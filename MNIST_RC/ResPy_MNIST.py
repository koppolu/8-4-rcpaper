# 8/14, Nihar Koppolu
# This is an example of ResevoirPy completing the MNIST task Successfully
#
# Had to tranform the prediction format from a 1 dimensional array of length 9 to an 8 * 9 array
#
# ReservoirPy may have more output neurons than pyRCN, may need to modify pyRCN's code. 
# This MNIST code is based on the ReservoirPy Japanese Speaker Classification Task and the pyRCN classification example.


from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np


from sklearn.model_selection import train_test_split
from pyrcn.datasets import load_digits

from reservoirpy import set_seed, verbosity
from reservoirpy.observables import nrmse, rsquare

from sklearn.metrics import accuracy_score

#from MNIST_pyRCN_SUCCESS import Y_pred_final

set_seed(42)
verbosity(0)


#Loading original data from pyRCN, This data is modified my the Create_MNIST functions
X, y = load_digits(return_X_y=True, as_sequence=True)
_X_train, M_X_test, M_y_train, M_y_test = train_test_split(X, y, test_size=0.2, random_state=42)


#This function is used to reshape X training and test data from a numpy array to a list containing numpy matrices
def X_Create_MNIST_X(M_X_train):
    #Modifying M_X_train to be a list
    M_X_train = M_X_train.tolist()

    #Debug
    #print("M_X_train[0]:", M_X_train[0])
    #print("type(M_X_train[0]):", type(M_X_train[0]))

    return M_X_train

#This function reformats y_train data from [0 0 0 1 0 0 0 0 0] -> [[0 0 0 1 0 0 0 0 0] [0 0 0 1 0 0 0 0 0] ... ] 
#(repeats 8 times to match the dimensions of the x input )
def Y_Create_MNIST_Y(M_y_train):

    #Modifying Y_train to be an array of 0's and 1's instead of a singular number
    Y_train_step_1 = []
    iter_range = len(M_y_train)
    for i in range(iter_range):
        y_array = np.zeros(10)
        add_1_index = M_y_train[i]
        y_array[add_1_index] = 1
        y_array = y_array.tolist()
        Y_train_step_1.append(y_array)

    #Giving Y_train_complete the same shape
    Y_train_complete = []
    for y_array in Y_train_step_1:
        temp_array = []
        for i in range(8): #copies and appends the array 8 times to make give each index the same dimensions as the test set
            temp_array.append(y_array)
            pass
        Y_train_complete.append(temp_array)

    for i in range(iter_range):
        test = np.array(Y_train_complete[i])
        Y_train_complete[i] = test 

    #DEBUG
    #print("np.array(Y_train_complete[0]):", Y_train_complete[0])
    #print("type(Y_train_complete[0]):", type(Y_train_complete[0]))

    return Y_train_complete


#This function turns the 8*9 Probability Array -> 9 index probability_array -> Prediction of which number is correct
def Process_Predicted_Values(Pred_Prob_Array):
    Final_Output_layer = []
    for i in range(10):
        Neuron_val = 0
        for k in range(8): 
            Neuron_val = Pred_Prob_Array[k][i] + Neuron_val
            pass
        Final_Output_layer.append(Neuron_val)
        pass

    return np.argmax(Final_Output_layer)



#Start of MAIN execution
#__________________________________________________________________________________________________________________
MNIST_X_train = X_Create_MNIST_X(_X_train)
MNIST_Y_train = Y_Create_MNIST_Y(M_y_train)

MNIST_X_test = X_Create_MNIST_X(M_X_test)
#MNIST_Y_test = Y_Create_MNIST_Y(M_y_test)

from reservoirpy.nodes import Reservoir, Ridge, Input

source = Input()
reservoir = Reservoir(500, sr=0.9, lr=0.1)
readout = Ridge(ridge=1e-6)

model = source >> reservoir >> readout



Y_pred = model.fit(MNIST_X_train, MNIST_Y_train, stateful=False, warmup=2).run(MNIST_X_test, stateful=False)

#print("Y_pred[0]", Y_pred[0]) For DEBUGGING
#print("MNIST_Y_test[0]", MNIST_Y_train[0])

#Creating array of Correct Answers
Y_test_final = []
for number in M_y_test:
    Y_test_final.append(number[0])
    pass

#Creating array of Predicted Answers
Y_pred_final = []
for set in Y_pred:
    Y_pred_final.append(Process_Predicted_Values(set))
    pass


score = accuracy_score(Y_test_final, Y_pred_final)
print("accuracy score: ", score)

from sklearn.model_selection import train_test_split
from pyrcn.datasets import load_digits
from pyrcn.echo_state_network import ESNClassifier
from sklearn.metrics import accuracy_score
import numpy as np



X, y = load_digits(return_X_y=True, as_sequence=True)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print("X_train len", len(X_train)) #1437 samples
print("y_train len", len(y_train))
print("X_test len", len(X_test)) #360
print("y_test len", len(y_test))

clf = ESNClassifier()
clf.fit(X=X_train, y=y_train)

y_pred_classes = clf.predict(X=X_test)  # output is the class for each input example
y_pred_proba = clf.predict_proba(X=X_test)  #  output are the class probabilities for each input example

Y_pred_final = []
Y_test_final = []
for i in range(360):
    #DEBUG print("Prediction Probability: ", np.argmax(y_pred_proba[i]))
    #print("actual Y value: ", y_test[i] )
    y_value = np.argmax(y_pred_proba[i])
    Y_pred_final.append(y_value)
    Y_test_final.append(y_test[i][0])


score = accuracy_score(Y_test_final, Y_pred_final)
print("accuracy score: ", score)
# Project Title: A Systematic Comparison of Reservoir Computing Frameworks <br> <br>
First Author:Nihar Koppolu, email: koppolu@pdx.edu <br>
2nd Author: Haranadh Chintapalli <br>
Advisor: Christof Teuscher <br>

## Purpose of Codebase: <br>
The purpose of this codebase is to test out the training speed and accuracy of different reservoir computing frameworks. <br>

The codebase consists of:<br> 

### main.py: 
- Used to run tests relating to the Mackey Glass data set.<br>
- The tests include: 
  1. Hyperparameter are set by Optimization Algorithm 
  2. Generate Basic Accuracy graphic showing how 100 node RC predicts after being trained with 100 timesteps: This is done just to ensure the framework works correctly 
  3. Timesteps to get to 0.05 nrmse for RC of all specified sizes - may do Monte Carlo runs to get more accurate result
  4. NRMSE vs Timestep graphic is generated for each RC
  5. Find Total Training Time to get to 0.05 nrmse for each RC along with std error, implemented with monte carlo runs 
	   
- The purpose of these different tests is to determine how increasing timesteps changes accuracy, and how long it takes to train. 
- The Basic Accuracy graphic was created to ensure that each framework runs as expected. 

### report_gen.py: 
- used to generate a Microsoft Word Document clearly displaying the results of all the tests in 1 place in an easily digestible format

### Data repository: 
- contains data in multiple files used by report_generator.py to generate report

### MNIST_RC repository:
- contains examples of the different Frameworks used to implement MNIST

### Framework_Superclass.py: 
- This is where all the tests for the frameworks are implemented. The superclass is used as a template for RC framework child classes used to test the child classes.
- This is also where the Mackey Glass Data is processed and stored

### RC framework child files: 
  - Fully compatible: ReservoirPy_class.py, pyRCN_class.py
  - Need to Update: Pytorch_ESN_class.py
  - Must add: Juilia Framwork (Hara), RCtorch

### Mackey_Glass.txt: 
- contains Mackey glass data

### preprocess_and_clean_data.py
- Mackey_Glass data is processed here 
- used to clean out all previously stored data
- has function to remove the useless reservoirpy hyperopt trial documentation

### config_tests.py
- contains Reservoir Size array used by main.py

### somewhat important files 
- will be explained in the next update to this README: torchesn, pytorch_Hyperopt_func.py

### Files to ignore: 
- Unit_Test.py, __pycache__, pytorch-esn <br><br>

If you have any questions about the code, **email me** at koppolu@pdx.edu or nihar.koppolu06@gmail.com. 

from cgi import test
import numpy as np
import matplotlib.pyplot as plt
#from easyesn import PredictionESN
#import timeit #used to time code
import time #used to time code
from reservoirpy.observables import mse, nrmse, rsquare
import statistics
from abc import ABC, abstractmethod
#Global Variables

import torch
#torch.set_deterministic(True)
#torch.use_deterministic_algorithms(True)
#torch.manual_seed(11663260459414417523)
print("Using PyTorch deterministic mode with seed: ", torch.manual_seed(0)) #Must set to 0, to ensure reproducibilty and correct function of Hyperparam opt function
import torch.nn as nn
from sklearn.linear_model import Ridge
from torchesn.nn import ESN as ESN
import numpy as np
import torch.nn
from torchesn import utils
device = torch.device('cpu')
dtype = torch.double
torch.set_default_dtype(dtype)

from Framework_Superclass import Reservoir_Framework
from preprocess_and_clean_data import delete_rpy_hyperopt_files 





class Pytorch(Reservoir_Framework): #superclass containing in brackets

    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize= True):
        super().__init__(Set_Num_Nodes, Framework_Name, Optimize) #must call init function of superclass

        if(self.Data_Set_Name == 'Mackey_Glass'):
            self.input_size = 1
            self.output_size = 1

        elif(self.Data_Set_Name == 'Double_Scroll'):
            self.input_size = 3
            self.output_size = 3

        self.hidden_size = self.reservoir_nodes
        
        self.loss_fcn = torch.nn.MSELoss()
        self.washout = [0]

       # self.hidden #set in Train function, used by test function


        if(self.optimize == True):
            self.best_params = self.Optimize_Hyperparam()
            pass
        #The case where optimize = False is only for UNIT TESTS
        else: 
            #These are params associated with 0.08 loss
            self.best_params = {'sr': 1.424, 'lr': 0.996, 'dens': 0.176}
            pass
           
        #Setting hyperparameters for reservoir computer
        self.model = ESN(
            self.input_size,
            self.hidden_size,
            self.output_size,
            spectral_radius= self.best_params['sr'],
            leaking_rate= self.best_params['lr'],
            density= self.best_params['dens'],
        )  
        pass
            

        #Setting Data for TorchEsn
        self.trX = torch.from_numpy(self.data_set_final[0:self.training_timesteps]).unsqueeze(1).to(device)
        self.trY = torch.from_numpy(self.data_set_final[1:self.training_timesteps + 1]).unsqueeze(1).to(device)
        self.opt_hyperparams_list = [self.reservoir_nodes, self.best_params['sr'], self.best_params['lr'], self.best_params['dens']]



        pass
    
    def Optimize_Hyperparam(self):
    #override abstract Optimize_Hyperparam

        #The objective function is used by ReservoirPy research function to determine hyperparameters
        def objective(dataset, config, *, N, sr, lr, dens):
            # You can access anything you put in the config 
            # file from the 'config' parameter.
            instances = config["instances_per_trial"]

            losses = []; r2s = [];
            for n in range(instances):
                # Build your model given the input parameters
                self.model = ESN(
                    self.input_size,
                    self.hidden_size,
                    self.output_size,
                    spectral_radius= sr,
                    leaking_rate= lr,
                    density= dens,
                ) 
                
                self.Train()
                self.test_results = self.Test()
                predictions = self.test_results['Y_pred']
                y_test = self.test_results['Y_actual']
                 
                loss = nrmse(y_test, predictions, norm_value=np.ptp(self.X_train))
                r2 = rsquare(y_test, predictions)
                
                
                losses.append(loss)
                r2s.append(r2)
                pass #end of FOR

            # Return a dictionnary of metrics. The 'loss' key is mandatory when
            # using hyperopt.
            return {'loss': np.mean(losses),
                    'r2': np.mean(r2s)}
            pass #End of objective function
        #END of OBJECTIVE func



        hyperopt_config = {
            "exp": f"hyperopt-multiscroll", # the experimentation name
            "hp_max_evals": 400,             # the number of differents sets of parameters hyperopt has to try
            "hp_method": "random",           # the method used by hyperopt to chose those sets (see below)
            "seed": 42,                      # the random state seed, to ensure reproducibility
            "instances_per_trial": 3,        # how many random ESN will be tried with each sets of parameters

            "hp_space": {                    # what are the ranges of parameters explored
                "N": ["choice", self.reservoir_nodes],            
                "sr": ["loguniform", 0.1, 2],   # the spectral radius is log-uniformly distributed between 0.1 and 2
                "lr": ["loguniform", 0.1, 1],  #leaking rate, from 0.1 to 1
                "dens": ["loguniform",0.1, 1], #density of connections
            }
        }  

        import json
        # we precautionously save the configuration in a JSON file
        # each file will begin with a number corresponding to the current experimentation run number.
        with open(f"{hyperopt_config['exp']}.config.json", "w+") as f:
            json.dump(hyperopt_config, f)
            

        from reservoirpy.hyper import research
        #creating 'dataset' which is used by the research function
        self.X_train = self.data_set_final[0:self.training_timesteps] #must add self for class variables
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1]

        x_test = self.data_set_final[self.test_set_begin : self.test_set_end]
        y_test = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1] 
        dataset = ((self.X_train, self.Y_train), (x_test, y_test))

        #This function completes the Hyperoptimization task 
        self.best = research(objective, dataset, f"{hyperopt_config['exp']}.config.json", ".")
        
        #Used to delete hyperopt_config files after running this function 
        delete_rpy_hyperopt_files()

        return self.best[0] #This contains a dictionary of values that were hyperoptimized 

        pass
        


    #override abstract Train method
    def Train(self):
    # Training
        #Must change trX and trY each time before training
        self.trX = torch.from_numpy(self.data_set_final[0:self.training_timesteps]).unsqueeze(1).to(device)
        self.trY = torch.from_numpy(self.data_set_final[1:self.training_timesteps + 1]).unsqueeze(1).to(device)

        trY_flat = utils.prepare_target(self.trY.clone(), [self.trX.size(0)], self.washout)
    
        #self.model = ESN(self.input_size, self.hidden_size, self.output_size)
        self.model.to(device)
    
        self.model(self.trX, self.washout, None, trY_flat)
        self.model.fit()
        output, self.hidden = self.model(self.trX, self.washout)
        pass



    def Test(self):    

        self.Test_X = torch.from_numpy(self.data_set_final[self.test_set_begin : self.test_set_end]).unsqueeze(1).to(device) 
        

        Y_actual = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1]
        
         #NEED TO FIX TEST
         #self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nmrse': self.calculated_nrmse}
        self.output, self.hidden = self.model(self.Test_X, [0], self.hidden)

        Y_pred = self.output.detach().numpy()
        Y_pred = np.squeeze(Y_pred)
        
        if(self.Data_Set_Name == 'Mackey_Glass'):
            Y_pred = Y_pred.reshape(-1,1)

        #NRMSE
        self.calculated_nrmse = nrmse(Y_pred, Y_actual)
        self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nrmse': self.calculated_nrmse}

        return self.test_results
         

   
    #override abstract Reset method
    def Reset_Reservoir(self):
        self.model = ESN(
            self.input_size,
            self.hidden_size,
            self.output_size,
            spectral_radius= self.best_params['sr'],
            leaking_rate= self.best_params['lr'],
            density= self.best_params['dens'],
            )    
        pass

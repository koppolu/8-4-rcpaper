import sys

#Test

SYSTEM_FRAMEWORK_CODE = input("Write the code for the system and framework that is being tested")

if(SYSTEM_FRAMEWORK_CODE == 'rhine_respy'):#ReservoirPy
    LIST_OF_FRAMEWORKS_TESTED = ['ReservoirPy']
elif(SYSTEM_FRAMEWORK_CODE == 'rhone_pytorch'): #pytorch
    LIST_OF_FRAMEWORKS_TESTED = ['Pytorch']
elif(SYSTEM_FRAMEWORK_CODE == 'rhone_julia'): #Julia
    LIST_OF_FRAMEWORKS_TESTED = ['ReservoirJulia']
elif(SYSTEM_FRAMEWORK_CODE == 'ticino_julia'): #Julia
    LIST_OF_FRAMEWORKS_TESTED = ['ReservoirJulia']
elif(SYSTEM_FRAMEWORK_CODE == 'saane_pyRCN'): #pyRCN
    LIST_OF_FRAMEWORKS_TESTED = ['pyRCN']
elif(SYSTEM_FRAMEWORK_CODE == 'rhone_pyRCN'): #pyRCN
    LIST_OF_FRAMEWORKS_TESTED = ['pyRCN']
elif(SYSTEM_FRAMEWORK_CODE == 'saane_RcTorch'): #RcTorch
    LIST_OF_FRAMEWORKS_TESTED = ['RcTorch']
elif(SYSTEM_FRAMEWORK_CODE == 'doubs_RcTorch'): #RcTorch
    LIST_OF_FRAMEWORKS_TESTED = ['RcTorch']

else:
    print("These tests are specifically meant to run on servers in Teuscher Lab located at Portland State University in the ECE department.\n")
    print("If you are trying to run this code elsewhere it is possible by making changes to this file and main.py.\nEmail me at koppolu@pdx.edu for specific questions")
    print("\nThis program will now terminate.")
    sys.exit()
    pass

RES_SIZE_SELECTOR = input("\nWrite whether you want to do a test of 100_200_nodes sizes or a test with reservoir computers of 100_to_10000 node sizes: ")

if (RES_SIZE_SELECTOR == '100_to_10000'):
    RES_SIZE = [100, 200, 300, 400, 450, 500, 750, 1000, 2000, 3500, 5000, 7500, 10000]
    pass

if (RES_SIZE_SELECTOR == '100_to_500_nodes'):
    RES_SIZE = [100, 200, 300, 400, 500]
    pass

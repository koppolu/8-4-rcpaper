from Framework_Superclass import Reservoir_Framework
from reservoirpy.observables import mse, nrmse, rmse
import csv
import numpy as np
from pyrcn.echo_state_network import ESNRegressor, ESNClassifier
from sklearn.linear_model import Ridge

#___________________________________________________________________________________________________________
#___________________________________________________________________________________________________________
#Framework 3: pyRCN (import of framework is placed in init)
class pyRCN(Reservoir_Framework): #superclass containing in brackets

    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize= True):

        super().__init__(Set_Num_Nodes, Framework_Name, Optimize) #must call init function of superclass

        #1. DEFINE INITIAL ESN MODEL  
        # Only fixing size of hidden layer
        self.initially_fixed_params = {'hidden_layer_size': self.reservoir_nodes, 'leakage': 1, 'spectral_radius': 0, 'input_scaling': 1}
                          #'input_activation': 'identity',
                          #'bias_scaling': 0.0,
                          #'reservoir_activation': 'tanh',
                          #'leakage': self.leakage_rate,
                          #'bidirectional': False,
                          #'k_rec': 10,
                          #'wash_out': 0,
                          #'continuation': False,
                          #'alpha': 1e-5,
                          #'random_state': 42,
                          #'requires_sequence': False,
                          #'spectral_radius': self.spectral_radius}
        
        self.esn = ESNRegressor(regressor=Ridge(), **self.initially_fixed_params)
        #old self.esn = ESNRegressor(bias_scaling=0, **initially_fixed_params)
        #old self.esn = ESNRegressor(regressor=Ridge(), **initially_fixed_params) 
        #old self.esn = ESNRegressor()
        
        #2. Calling Optimization function
        if(self.optimize == True):
            print("\n\nCalling optimization function\n\n")
            self.best_params = self.Optimize_Hyperparam()
            print("Hyperparameter optimization complete")
            self.esn = ESNRegressor(regressor=Ridge(), **self.best_params) 
            pass

        #if No hyperopt UNIT TESTING
        else: 
            self.best_params = {'hidden_layer_size': self.reservoir_nodes, 'leakage': 0.9, 'spectral_radius': 0.34, 'input_scaling': 0.09}            
            self.esn = ESNRegressor(regressor=Ridge(), **self.best_params)
            pass

        #List of Hyperparam Optimized
        self.opt_hyperparams_list = [self.best_params['hidden_layer_size'], self.best_params['spectral_radius'], self.best_params['leakage'], self.best_params['input_scaling']]

        pass

    #override abstract Optimize_Hyperparam method: NEED TO CHANGE step_2_params when running tests
    def Optimize_Hyperparam(self):

        from sklearn.metrics import make_scorer
        from sklearn.metrics import mean_squared_error
        from sklearn.model_selection import TimeSeriesSplit
        from sklearn.model_selection import (RandomizedSearchCV, GridSearchCV)
        from scipy.stats import uniform
        from pyrcn.model_selection import SequentialSearchCV

        #1. Define optimization workflow
        step1_esn_params = {'input_scaling': np.linspace(0.1, 5.0, 50),
                            'spectral_radius': np.linspace(0.0, 1.5, 16)}
        step2_esn_params = {'leakage': np.linspace(0.1, 1.0, 10)}
        step3_esn_params = {'bias_scaling': np.linspace(0.0, 1.5, 16)}

        scorer = make_scorer(score_func=mean_squared_error, greater_is_better=False)

        kwargs = {'verbose': 5,
                'scoring': scorer,
                'n_jobs': -1,
                'cv': TimeSeriesSplit()}


        searches = [('step1', GridSearchCV, step1_esn_params, kwargs),
                    ('step2', GridSearchCV, step2_esn_params, kwargs),
                    ('step3', GridSearchCV, step3_esn_params, kwargs)]
            #
        #2. PERFORM THE SEARCH: find bet params
        self.X_train = self.data_set_final[0:self.training_timesteps] #must add self for class variables
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1] 
        #self.sequential_search_esn = SequentialSearchCV(self.esn, searches).fit(self.X_train.reshape(-1, 1), self.Y_train)
        self.sequential_search_esn = SequentialSearchCV(self.esn, searches).fit(self.X_train, self.Y_train)
        best_params = self.sequential_search_esn.best_estimator_.get_params()
        return best_params

        pass


    #override abstract Train method
    def Train(self):
        
        self.X_train = self.data_set_final[0:self.training_timesteps] #must add self for class variables
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1] 

        #self.esn.fit(self.X_train.reshape(-1, 1), self.Y_train)
        self.esn.fit(self.X_train, self.Y_train) 


        pass
   
    #override abstract Test method
    def Test(self):

        Y_pred = self.esn.predict(self.data_set_final[self.test_set_begin : self.test_set_end])
        Y_actual = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1]

        #this is used to calculate nrmse and mse
        self.calculated_nrmse = nrmse(Y_pred, Y_actual)
        self.calculated_mse = (Y_pred, Y_actual)

        self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nrmse': self.calculated_nrmse}

        return self.test_results

    #override abstract Reset method
    def Reset_Reservoir(self):

        from pyrcn.echo_state_network import ESNRegressor, ESNClassifier
        from sklearn.linear_model import Ridge

        #RESETS WITH OPTIMIZED Hyperparameters
        self.esn = ESNRegressor(regressor=Ridge(), **self.best_params) 

        pass

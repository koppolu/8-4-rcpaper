from Framework_Superclass import Reservoir_Framework
from reservoirpy.observables import mse, nrmse, rmse, rsquare
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from reservoirpy.nodes import Reservoir, Ridge
import numpy as np

from preprocess_and_clean_data import delete_rpy_hyperopt_files
import os

#___________________________________________________________________________________________________________
#___________________________________________________________________________________________________________
#Framework 1: ReservoirPy 
class ReservoirPy(Reservoir_Framework): #superclass containing in brackets

    def __init__(self, Set_Num_Nodes, Framework_Name, Optimize= True):
        
        super().__init__(Set_Num_Nodes, Framework_Name, Optimize) #must call init function of superclass

        #Process MNIST dataset if MNIST is being used


        self.final_X_coord_digit_set = [] #Only given value if MNIST data set is being processed
        self.final_Y_actual_value = []
        if (self.Data_Set_Name == 'MNIST'):
            self.final_X_coord_digit_set, self.final_Y_actual_value = self.ResPy_Process_MNIST()
            pass
        
        #Optimize set to TRUE when collecting results
        if(self.optimize == True):
            self.best_params = self.Optimize_Hyperparam()

            self.reservoir = Reservoir(self.reservoir_nodes, 
                                        sr=self.best_params['sr'], 
                                        lr=self.best_params['lr'], 
                                        inut_scaling=self.best_params['iss'], 
                                        seed=1234)
            
            pass
        
        #The case where optimize = False is only for UNIT TESTS
        else: 
            self.reservoir = Reservoir(self.reservoir_nodes, lr=0.605, sr=0.22)
            self.best_params = {'lr': 0.605, 'sr': 0.22, 'iss': -1}
        
        #List of Hyperparam optimized
        self.opt_hyperparams_list = [self.reservoir_nodes, self.best_params['sr'], self.best_params['lr'], self.best_params['iss']]
        
        if (self.Data_Set_Name != 'MNIST'): self.readout = Ridge(ridge=1e-7)
        else: self.readout = Ridge(ridge=1e-6)

        self.esn_model = self.reservoir >> self.readout #connects reservoir and ridge

        pass

#Processes Raw MNIST data form into form readable by ReservoirPy: Called from INIT only
    def ResPy_Process_MNIST(self):
        #Processing set of digits in coordinate form (X values) 
        self.final_X_coord_digit_set = self.X_coord_digit_set.tolist()

        #Processing actual values of digits (Y values)
        #Modifying Y_train to be an array of 0's and 1's instead of a singular number
        Y_step_1 = []
        iter_range = len(self.Y_actual_digit_set)
        for i in range(iter_range):
            y_array = np.zeros(10)
            add_1_index = self.Y_actual_digit_set[i]
            y_array[add_1_index] = 1
            y_array = y_array.tolist()
            Y_step_1.append(y_array)

        #Giving Y_complete the same shape
        Y_complete = []
        for y_array in Y_step_1:
            temp_array = []
            for i in range(8): #copies and appends the array 8 times to make give each index the same dimensions as the test set
                temp_array.append(y_array)
                pass
            Y_complete.append(temp_array)

        for i in range(iter_range):
            test = np.array(Y_complete[i])
            Y_complete[i] = test 

        self.final_Y_actual_value = Y_complete

        return self.final_X_coord_digit_set, self.final_Y_actual_value



    #override abstract Optimize_Hyperparam
    def Optimize_Hyperparam(self):


        if(self.Data_Set_Name == 'MNIST'):
            input("\nMNIST hyperopt not implemented yet\n\n")
            pass

        #The objective function is used by ReservoirPy research function to determine hyperparameters
        def objective(dataset, config, *, iss, N, sr, lr, ridge, seed):
            # You can access anything you put in the config 
            # file from the 'config' parameter.
            instances = config["instances_per_trial"]
            
            # The seed should be changed across the instances, 
            # to be sure there is no bias in the results 
            # due to initialization.
            variable_seed = seed 

            losses = []; r2s = [];
            for n in range(instances):
                # Build your model given the input parameters
                reservoir = Reservoir(N, 
                                    sr=sr, 
                                    lr=lr, 
                                    inut_scaling=iss, 
                                    seed=variable_seed)
                
                readout = Ridge(ridge=ridge)

                model = reservoir >> readout

                x_test = self.data_set_final[self.test_set_begin : self.test_set_end]
                y_test = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1] 

                # Train your model and test your model.
                predictions = model.fit(self.X_train, self.Y_train) \
                                .run(x_test)
                
                loss = nrmse(y_test, predictions, norm_value=np.ptp(self.X_train))
                r2 = rsquare(y_test, predictions)
                
                # Change the seed between instances
                variable_seed += 1
                
                losses.append(loss)
                r2s.append(r2)
                pass #end of FOR

            # Return a dictionnary of metrics. The 'loss' key is mandatory when
            # using hyperopt.
            return {'loss': np.mean(losses),
                    'r2': np.mean(r2s)}
            pass #End of objective function
        #END of OBJECTIVE func

        hyperopt_config = {
            "exp": f"hyperopt-multiscroll", # the experimentation name
            "hp_max_evals": 200,             # the number of differents sets of parameters hyperopt has to try
            "hp_method": "random",           # the method used by hyperopt to chose those sets (see below)
            "seed": 42,                      # the random state seed, to ensure reproducibility
            "instances_per_trial": 3,        # how many random ESN will be tried with each sets of parameters

            "hp_space": {                    # what are the ranges of parameters explored
                "N": ["choice", self.reservoir_nodes],             # the number of neurons is fixed to 500
                "sr": ["loguniform", 1e-2, 2],   # the spectral radius is log-uniformly distributed between 1e-2 and 2
                "lr": ["loguniform", 1e-3, 1],  # idem with the leaking rate, from 1e-3 to 1
                "iss": ["loguniform",1e-2, 2],           # the input scaling is fixed #MAY CAUSE ERROR
                "ridge": ["choice", 1e-7],        # and so is the regularization parameter.
                "seed": ["choice", 1234]          # an other random seed for the ESN initialization
            }
        }  

        import json
        # we precautionously save the configuration in a JSON file
        # each file will begin with a number corresponding to the current experimentation run number.
        with open(f"{hyperopt_config['exp']}.config.json", "w+") as f:
            json.dump(hyperopt_config, f)
            

        from reservoirpy.hyper import research
        #creating 'dataset' which is used by the research function
        self.X_train = self.data_set_final[0:self.training_timesteps] #must add self for class variables
        self.Y_train = self.data_set_final[1:self.training_timesteps + 1]

        x_test = self.data_set_final[self.test_set_begin : self.test_set_end]
        y_test = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1] 
        dataset = ((self.X_train, self.Y_train), (x_test, y_test))

        #This function completes the Hyperoptimization task 
        self.best = research(objective, dataset, f"{hyperopt_config['exp']}.config.json", ".")
        
        #Used to delete hyperopt_config files after running this function 
        delete_rpy_hyperopt_files()

        return self.best[0] #This contains a dictionary of values that were hyperoptimized 

        pass



    #override abstract Train method
    def Train(self):
        
        if(self.Data_Set_Name != 'MNIST'):
            self.X_train = self.data_set_final[0:self.training_timesteps] #must add self for class variables
            self.Y_train = self.data_set_final[1:self.training_timesteps + 1] 
            pass

        else: #MNIST
            self.X_train = self.final_X_coord_digit_set[0:self.training_timesteps] #must add self for class variables
            self.Y_train = self.final_Y_actual_value[0:self.training_timesteps] 

        self.esn_model = self.esn_model.fit(self.X_train, self.Y_train, warmup=0) #training RC

        pass

    
    #override abstract Test method
    def Test(self):

        
        if(self.Data_Set_Name != 'MNIST'): #Mackey and Double Scroll
            Y_pred = self.esn_model.run(self.data_set_final[self.test_set_begin : self.test_set_end])
            Y_actual = self.data_set_final[self.test_set_begin + 1 : self.test_set_end + 1]


            self.calculated_nrmse = nrmse(Y_pred, Y_actual)

            self.test_results = {'Y_pred': Y_pred, 'Y_actual': Y_actual, 'nrmse': self.calculated_nrmse}
            pass

        #####Copied
        else: #MNIST
            Y_pred = self.esn_model.run(self.final_X_coord_digit_set[self.test_set_begin : self.test_set_end], stateful=False)

            #Creating array of Correct Answers
            Y_test_final = []
            for number in self.Y_actual_digit_set[self.test_set_begin : self.test_set_end]:
                Y_test_final.append(number[0])
                pass

            #This function turns the 8*9 Probability Array -> 9 index probability_array -> Prediction of which number is correct
            def Process_Predicted_Values(Pred_Prob_Array):
                Final_Output_layer = []
                for i in range(10):
                    Neuron_val = 0
                    for k in range(8): 
                        Neuron_val = Pred_Prob_Array[k][i] + Neuron_val
                        pass
                    Final_Output_layer.append(Neuron_val)
                    pass

                return np.argmax(Final_Output_layer)

            #Creating array of Predicted Answers
            Y_pred_final = []
            for set in Y_pred:
                Y_pred_final.append(Process_Predicted_Values(set))
                pass

            #print('Y_test_final[0]',Y_test_final[0:5])
            #print('Y_pred_final[0]',Y_pred_final[0:5])

            score = accuracy_score(Y_test_final, Y_pred_final)
            self.error_rate = 100 - score
            self.test_results = {'Y_pred': Y_pred_final, 'Y_actual': Y_test_final, 'error_rate': self.error_rate}
            pass

        #####

        
        #this is used to calculate nrmse and mse

        return self.test_results




    #override abstract Reset method
    def Reset_Reservoir(self):
        from reservoirpy.nodes import Ridge
        
        if (self.Data_Set_Name != 'MNIST'): self.readout = Ridge(ridge=1e-7)
        else: self.readout = Ridge(ridge=1e-6)
        self.esn_model = self.reservoir >> self.readout #connects reservoir and ridge

        pass

